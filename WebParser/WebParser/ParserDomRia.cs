﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebParser.Interfaces;

namespace WebParser
{
    struct Property
    {
        string Title { get; set; }
        string Value { get; set; }
    }

    class ParserDomRia : IParser
    {
        string[] code;
        List<string> propertyText;
        private readonly string[] tagWords;
        List<Property> properties;

        public ParserDomRia(string[] code)
        {
            this.code = code;
            properties = new List<Property>();
            tagWords = new string[] {
                "a", "dt", "dd", "span", "i", "class",
                "<!--", "!--", "-->", "--", "strong", "br", "p", "footer", "ul", "style",
                "}", "/"
            };

            propertyText = GetTextWithoutTags(); // GetTextForProperty();
        }

        public List<string> GetTextWithoutTags()
        {
            List<string> newText = new List<string>();

            for (int wordIndex = 0; wordIndex < code.Length; ++wordIndex)
            {
                code[wordIndex] = code[wordIndex].Trim();
                if (!IsContainTag(code[wordIndex]) && code[wordIndex] != "")
                {
                    newText.Add(code[wordIndex] + '\n');
                }
            }

            return newText;
        }

        public List<string> GetTextForProperty()
        {
            List<string> newText = new List<string>();
            string[] textWithoutTags = GetTextWithoutTags().ToArray();

            string word, prevWord;
            for (int wordIndex = 0; wordIndex < textWithoutTags.Length; ++wordIndex)
            {
                word = textWithoutTags[wordIndex];
                prevWord = (wordIndex == 0) ? "" : textWithoutTags[wordIndex - 1];
                if (IsTitleOrValue(word, prevWord))
                {
                    newText.Add(textWithoutTags[wordIndex] + '\n');
                }
            }

            return newText;
        }

        private bool IsContainTag(string word)
        {
            foreach (string tag in tagWords)
            {
                if (word.IndexOf(tag) != -1)
                {
                    return true;
                }
            }

            return false;
        }

        private bool IsTitleOrValue(string word, string prevWord)
        {
            if (word.IndexOf(":") != -1 || prevWord.IndexOf(":") != -1)
            {
                return true;
            }

            return false;
        }

        public List<Property> GetProperties()
        {
            throw new NotImplementedException();
        }
    }
}
