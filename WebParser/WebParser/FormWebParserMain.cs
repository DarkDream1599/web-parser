﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WebParser
{
    public partial class FormWebParserMain : Form
    {
        string url = "https://dom.ria.com/ru/realty-perevireno-prodaja-dom-vinnitsa-pirogovo-akademichniy-provulok-13576897.html";

        public FormWebParserMain()
        {
            InitializeComponent();
        }

        public static String GetCode(string urlAddress)
        {
            string data = "";
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(urlAddress);
            HttpWebResponse response = (HttpWebResponse)request.GetResponse();
            if (response.StatusCode == HttpStatusCode.OK)
            {
                Stream receiveStream = response.GetResponseStream();
                StreamReader readStream = null;
                if (response.CharacterSet == null)
                {
                    readStream = new StreamReader(receiveStream);
                }
                else
                {
                    readStream = new StreamReader(receiveStream, Encoding.GetEncoding(response.CharacterSet));
                }
                data = readStream.ReadToEnd();
                response.Close();
                readStream.Close();
            }
            return data;
        }

        private void buttonParse_Click(object sender, EventArgs e)
        {
            string text = GetCode(url);
            text = text.ToLower();
            // text = text.Substring(text.LastIndexOf("характеристики"));
            // text = text.Substring(0, text.IndexOf("создано:"));

            string[] lines = text.Split(new char[] { '\n', '\r', '>', '<' }, StringSplitOptions.RemoveEmptyEntries);

            ParserDomRia parser = new ParserDomRia(lines);
            File.WriteAllLines("out.txt", parser.GetTextWithoutTags());

            MessageBox.Show("It has parsed", "OK");
        }

        private void menuResultOpenTable_Click(object sender, EventArgs e)
        {
            Form formResult = new FormWebParserResult();
            formResult.Show();
        }
    }
}
