﻿namespace WebParser
{
    partial class FormWebParserResult
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormWebParserResult));
            this.grid = new System.Windows.Forms.DataGridView();
            this.columnNumber = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.columnType = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.columnRooms = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.columnFloor = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.columnCountFloor = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.columnGeneralSquare = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.columnLivingSquare = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.columnKitchenSquare = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.columnPropositionType = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.columnWallType = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.columnBuildYear = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.columnAddress = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.columnElse = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.columnLink = new System.Windows.Forms.DataGridViewLinkColumn();
            ((System.ComponentModel.ISupportInitialize)(this.grid)).BeginInit();
            this.SuspendLayout();
            // 
            // grid
            // 
            this.grid.AllowUserToAddRows = false;
            this.grid.AllowUserToDeleteRows = false;
            this.grid.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.grid.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.grid.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.grid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.columnNumber,
            this.columnType,
            this.columnRooms,
            this.columnFloor,
            this.columnCountFloor,
            this.columnGeneralSquare,
            this.columnLivingSquare,
            this.columnKitchenSquare,
            this.columnPropositionType,
            this.columnWallType,
            this.columnBuildYear,
            this.columnAddress,
            this.columnElse,
            this.columnLink});
            this.grid.Location = new System.Drawing.Point(0, 0);
            this.grid.Name = "grid";
            this.grid.ReadOnly = true;
            this.grid.Size = new System.Drawing.Size(948, 415);
            this.grid.TabIndex = 0;
            // 
            // columnNumber
            // 
            this.columnNumber.HeaderText = "#";
            this.columnNumber.Name = "columnNumber";
            this.columnNumber.ReadOnly = true;
            // 
            // columnType
            // 
            this.columnType.HeaderText = "Тип";
            this.columnType.Name = "columnType";
            this.columnType.ReadOnly = true;
            // 
            // columnRooms
            // 
            this.columnRooms.HeaderText = "Комнат";
            this.columnRooms.Name = "columnRooms";
            this.columnRooms.ReadOnly = true;
            // 
            // columnFloor
            // 
            this.columnFloor.HeaderText = "Этаж";
            this.columnFloor.Name = "columnFloor";
            this.columnFloor.ReadOnly = true;
            // 
            // columnCountFloor
            // 
            this.columnCountFloor.HeaderText = "Этажность";
            this.columnCountFloor.Name = "columnCountFloor";
            this.columnCountFloor.ReadOnly = true;
            // 
            // columnGeneralSquare
            // 
            this.columnGeneralSquare.HeaderText = "Общая пл.";
            this.columnGeneralSquare.Name = "columnGeneralSquare";
            this.columnGeneralSquare.ReadOnly = true;
            // 
            // columnLivingSquare
            // 
            this.columnLivingSquare.HeaderText = "Жилая пл.";
            this.columnLivingSquare.Name = "columnLivingSquare";
            this.columnLivingSquare.ReadOnly = true;
            // 
            // columnKitchenSquare
            // 
            this.columnKitchenSquare.HeaderText = "Кухонная пл.";
            this.columnKitchenSquare.Name = "columnKitchenSquare";
            this.columnKitchenSquare.ReadOnly = true;
            // 
            // columnPropositionType
            // 
            this.columnPropositionType.HeaderText = "Тип пропозиции";
            this.columnPropositionType.Name = "columnPropositionType";
            this.columnPropositionType.ReadOnly = true;
            // 
            // columnWallType
            // 
            this.columnWallType.HeaderText = "Тип стен";
            this.columnWallType.Name = "columnWallType";
            this.columnWallType.ReadOnly = true;
            // 
            // columnBuildYear
            // 
            this.columnBuildYear.HeaderText = "Год постройки";
            this.columnBuildYear.Name = "columnBuildYear";
            this.columnBuildYear.ReadOnly = true;
            // 
            // columnAddress
            // 
            this.columnAddress.HeaderText = "Адрес";
            this.columnAddress.Name = "columnAddress";
            this.columnAddress.ReadOnly = true;
            // 
            // columnElse
            // 
            this.columnElse.HeaderText = "Остальное";
            this.columnElse.Name = "columnElse";
            this.columnElse.ReadOnly = true;
            // 
            // columnLink
            // 
            this.columnLink.HeaderText = "Ссылка";
            this.columnLink.Name = "columnLink";
            this.columnLink.ReadOnly = true;
            // 
            // FormWebParserResult
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(948, 415);
            this.Controls.Add(this.grid);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FormWebParserResult";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "FormWebParserResult";
            ((System.ComponentModel.ISupportInitialize)(this.grid)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView grid;
        private System.Windows.Forms.DataGridViewTextBoxColumn columnNumber;
        private System.Windows.Forms.DataGridViewTextBoxColumn columnType;
        private System.Windows.Forms.DataGridViewTextBoxColumn columnRooms;
        private System.Windows.Forms.DataGridViewTextBoxColumn columnFloor;
        private System.Windows.Forms.DataGridViewTextBoxColumn columnCountFloor;
        private System.Windows.Forms.DataGridViewTextBoxColumn columnGeneralSquare;
        private System.Windows.Forms.DataGridViewTextBoxColumn columnLivingSquare;
        private System.Windows.Forms.DataGridViewTextBoxColumn columnKitchenSquare;
        private System.Windows.Forms.DataGridViewTextBoxColumn columnPropositionType;
        private System.Windows.Forms.DataGridViewTextBoxColumn columnWallType;
        private System.Windows.Forms.DataGridViewTextBoxColumn columnBuildYear;
        private System.Windows.Forms.DataGridViewTextBoxColumn columnAddress;
        private System.Windows.Forms.DataGridViewTextBoxColumn columnElse;
        private System.Windows.Forms.DataGridViewLinkColumn columnLink;
    }
}