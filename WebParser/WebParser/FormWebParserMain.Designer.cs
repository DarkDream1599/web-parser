﻿namespace WebParser
{
    partial class FormWebParserMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormWebParserMain));
            this.buttonParse = new System.Windows.Forms.Button();
            this.panelSearch = new System.Windows.Forms.Panel();
            this.labelSearch = new System.Windows.Forms.Label();
            this.comboBoxSeach = new System.Windows.Forms.ComboBox();
            this.comboBoxTypes = new System.Windows.Forms.ComboBox();
            this.lableOperation = new System.Windows.Forms.Label();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.comboBox2 = new System.Windows.Forms.ComboBox();
            this.labelRooms = new System.Windows.Forms.Label();
            this.labelSquare = new System.Windows.Forms.Label();
            this.labelPrice = new System.Windows.Forms.Label();
            this.labelTypePrice = new System.Windows.Forms.Label();
            this.numericRoomsFrom = new System.Windows.Forms.NumericUpDown();
            this.numericRoomsTo = new System.Windows.Forms.NumericUpDown();
            this.numericSquareTo = new System.Windows.Forms.NumericUpDown();
            this.numericSquareFrom = new System.Windows.Forms.NumericUpDown();
            this.numericPriceTo = new System.Windows.Forms.NumericUpDown();
            this.numericPriceFrom = new System.Windows.Forms.NumericUpDown();
            this.comboBox3 = new System.Windows.Forms.ComboBox();
            this.labelSeparator = new System.Windows.Forms.Label();
            this.menu = new System.Windows.Forms.MenuStrip();
            this.menuFile = new System.Windows.Forms.ToolStripMenuItem();
            this.menuFileImportTo = new System.Windows.Forms.ToolStripMenuItem();
            this.menuFileImportToTextDocument = new System.Windows.Forms.ToolStripMenuItem();
            this.menuFileImportToExcel = new System.Windows.Forms.ToolStripMenuItem();
            this.menuSeparator = new System.Windows.Forms.ToolStripSeparator();
            this.menuExit = new System.Windows.Forms.ToolStripMenuItem();
            this.menuResult = new System.Windows.Forms.ToolStripMenuItem();
            this.menuResultOpenTable = new System.Windows.Forms.ToolStripMenuItem();
            this.panelSearch.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericRoomsFrom)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericRoomsTo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericSquareTo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericSquareFrom)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericPriceTo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericPriceFrom)).BeginInit();
            this.menu.SuspendLayout();
            this.SuspendLayout();
            // 
            // buttonParse
            // 
            this.buttonParse.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.buttonParse.Location = new System.Drawing.Point(298, 379);
            this.buttonParse.Name = "buttonParse";
            this.buttonParse.Size = new System.Drawing.Size(112, 38);
            this.buttonParse.TabIndex = 1;
            this.buttonParse.Text = "Вытянуть";
            this.buttonParse.UseVisualStyleBackColor = true;
            this.buttonParse.Click += new System.EventHandler(this.buttonParse_Click);
            // 
            // panelSearch
            // 
            this.panelSearch.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.panelSearch.Controls.Add(this.labelSeparator);
            this.panelSearch.Controls.Add(this.comboBox3);
            this.panelSearch.Controls.Add(this.numericPriceTo);
            this.panelSearch.Controls.Add(this.numericPriceFrom);
            this.panelSearch.Controls.Add(this.numericSquareTo);
            this.panelSearch.Controls.Add(this.numericSquareFrom);
            this.panelSearch.Controls.Add(this.numericRoomsTo);
            this.panelSearch.Controls.Add(this.numericRoomsFrom);
            this.panelSearch.Controls.Add(this.labelTypePrice);
            this.panelSearch.Controls.Add(this.labelPrice);
            this.panelSearch.Controls.Add(this.labelSquare);
            this.panelSearch.Controls.Add(this.labelRooms);
            this.panelSearch.Controls.Add(this.comboBox2);
            this.panelSearch.Controls.Add(this.label1);
            this.panelSearch.Controls.Add(this.comboBox1);
            this.panelSearch.Controls.Add(this.lableOperation);
            this.panelSearch.Controls.Add(this.comboBoxTypes);
            this.panelSearch.Controls.Add(this.comboBoxSeach);
            this.panelSearch.Controls.Add(this.labelSearch);
            this.panelSearch.Location = new System.Drawing.Point(12, 36);
            this.panelSearch.Name = "panelSearch";
            this.panelSearch.Size = new System.Drawing.Size(669, 322);
            this.panelSearch.TabIndex = 2;
            // 
            // labelSearch
            // 
            this.labelSearch.AutoSize = true;
            this.labelSearch.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelSearch.ForeColor = System.Drawing.Color.White;
            this.labelSearch.Location = new System.Drawing.Point(25, 32);
            this.labelSearch.Name = "labelSearch";
            this.labelSearch.Size = new System.Drawing.Size(65, 19);
            this.labelSearch.TabIndex = 0;
            this.labelSearch.Text = "Поиск:";
            // 
            // comboBoxSeach
            // 
            this.comboBoxSeach.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.comboBoxSeach.FormattingEnabled = true;
            this.comboBoxSeach.Items.AddRange(new object[] {
            "  Квартиры"});
            this.comboBoxSeach.Location = new System.Drawing.Point(135, 30);
            this.comboBoxSeach.Name = "comboBoxSeach";
            this.comboBoxSeach.Size = new System.Drawing.Size(211, 24);
            this.comboBoxSeach.TabIndex = 1;
            this.comboBoxSeach.Text = "  Квартиры";
            // 
            // comboBoxTypes
            // 
            this.comboBoxTypes.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.comboBoxTypes.FormattingEnabled = true;
            this.comboBoxTypes.Items.AddRange(new object[] {
            "  Все типы",
            "  Квартира",
            "  Комната"});
            this.comboBoxTypes.Location = new System.Drawing.Point(404, 30);
            this.comboBoxTypes.Name = "comboBoxTypes";
            this.comboBoxTypes.Size = new System.Drawing.Size(248, 24);
            this.comboBoxTypes.TabIndex = 1;
            this.comboBoxTypes.Text = "  Все типы";
            // 
            // lableOperation
            // 
            this.lableOperation.AutoSize = true;
            this.lableOperation.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lableOperation.ForeColor = System.Drawing.Color.White;
            this.lableOperation.Location = new System.Drawing.Point(25, 101);
            this.lableOperation.Name = "lableOperation";
            this.lableOperation.Size = new System.Drawing.Size(87, 19);
            this.lableOperation.TabIndex = 2;
            this.lableOperation.Text = "Операция:";
            // 
            // comboBox1
            // 
            this.comboBox1.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Items.AddRange(new object[] {
            "Продажа"});
            this.comboBox1.Location = new System.Drawing.Point(135, 101);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(211, 24);
            this.comboBox1.TabIndex = 3;
            this.comboBox1.Text = "  Продажа";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(25, 155);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(88, 19);
            this.label1.TabIndex = 4;
            this.label1.Text = "В области:";
            // 
            // comboBox2
            // 
            this.comboBox2.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.comboBox2.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.comboBox2.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.comboBox2.FormattingEnabled = true;
            this.comboBox2.Items.AddRange(new object[] {
            "  Выберите область",
            "  Винницкая",
            "  Волынская",
            "  Днепропетровская",
            "  Донецкая",
            "  Житомирская",
            "  Закарпатская",
            "  Запорожская",
            "  Ивано-Франковская",
            "  Киевская",
            "  Кировоградская",
            "  Луганская",
            "  Львовская",
            "  Николаевская",
            "  Одесская",
            "  Полтавская",
            "  Республика Крым",
            "  Ровенская",
            "  Сумская",
            "  Тернопольская",
            "  Харьковская",
            "  Херсонская",
            "  Хмельницкая",
            "  Черкасская",
            "  Черниговская",
            "  Черновицкая"});
            this.comboBox2.Location = new System.Drawing.Point(135, 155);
            this.comboBox2.Name = "comboBox2";
            this.comboBox2.Size = new System.Drawing.Size(211, 24);
            this.comboBox2.TabIndex = 5;
            this.comboBox2.Text = "  Выберите область";
            // 
            // labelRooms
            // 
            this.labelRooms.AutoSize = true;
            this.labelRooms.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelRooms.ForeColor = System.Drawing.Color.White;
            this.labelRooms.Location = new System.Drawing.Point(404, 101);
            this.labelRooms.Name = "labelRooms";
            this.labelRooms.Size = new System.Drawing.Size(69, 19);
            this.labelRooms.TabIndex = 6;
            this.labelRooms.Text = "Комнат:";
            // 
            // labelSquare
            // 
            this.labelSquare.AutoSize = true;
            this.labelSquare.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelSquare.ForeColor = System.Drawing.Color.White;
            this.labelSquare.Location = new System.Drawing.Point(404, 155);
            this.labelSquare.Name = "labelSquare";
            this.labelSquare.Size = new System.Drawing.Size(79, 19);
            this.labelSquare.TabIndex = 7;
            this.labelSquare.Text = "Общая S:";
            // 
            // labelPrice
            // 
            this.labelPrice.AutoSize = true;
            this.labelPrice.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelPrice.ForeColor = System.Drawing.Color.White;
            this.labelPrice.Location = new System.Drawing.Point(404, 208);
            this.labelPrice.Name = "labelPrice";
            this.labelPrice.Size = new System.Drawing.Size(51, 19);
            this.labelPrice.TabIndex = 8;
            this.labelPrice.Text = "Цена:";
            // 
            // labelTypePrice
            // 
            this.labelTypePrice.AutoSize = true;
            this.labelTypePrice.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelTypePrice.ForeColor = System.Drawing.Color.White;
            this.labelTypePrice.Location = new System.Drawing.Point(404, 261);
            this.labelTypePrice.Name = "labelTypePrice";
            this.labelTypePrice.Size = new System.Drawing.Size(104, 19);
            this.labelTypePrice.TabIndex = 9;
            this.labelTypePrice.Text = "Тип валюты:";
            // 
            // numericRoomsFrom
            // 
            this.numericRoomsFrom.Location = new System.Drawing.Point(521, 100);
            this.numericRoomsFrom.Name = "numericRoomsFrom";
            this.numericRoomsFrom.Size = new System.Drawing.Size(58, 20);
            this.numericRoomsFrom.TabIndex = 10;
            // 
            // numericRoomsTo
            // 
            this.numericRoomsTo.Location = new System.Drawing.Point(594, 100);
            this.numericRoomsTo.Name = "numericRoomsTo";
            this.numericRoomsTo.Size = new System.Drawing.Size(58, 20);
            this.numericRoomsTo.TabIndex = 11;
            // 
            // numericSquareTo
            // 
            this.numericSquareTo.Location = new System.Drawing.Point(594, 154);
            this.numericSquareTo.Name = "numericSquareTo";
            this.numericSquareTo.Size = new System.Drawing.Size(58, 20);
            this.numericSquareTo.TabIndex = 13;
            // 
            // numericSquareFrom
            // 
            this.numericSquareFrom.Location = new System.Drawing.Point(521, 154);
            this.numericSquareFrom.Name = "numericSquareFrom";
            this.numericSquareFrom.Size = new System.Drawing.Size(58, 20);
            this.numericSquareFrom.TabIndex = 12;
            // 
            // numericPriceTo
            // 
            this.numericPriceTo.Location = new System.Drawing.Point(594, 207);
            this.numericPriceTo.Name = "numericPriceTo";
            this.numericPriceTo.Size = new System.Drawing.Size(58, 20);
            this.numericPriceTo.TabIndex = 15;
            // 
            // numericPriceFrom
            // 
            this.numericPriceFrom.Location = new System.Drawing.Point(521, 207);
            this.numericPriceFrom.Name = "numericPriceFrom";
            this.numericPriceFrom.Size = new System.Drawing.Size(58, 20);
            this.numericPriceFrom.TabIndex = 14;
            // 
            // comboBox3
            // 
            this.comboBox3.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.comboBox3.FormattingEnabled = true;
            this.comboBox3.Items.AddRange(new object[] {
            "  $",
            "  грн",
            "  €"});
            this.comboBox3.Location = new System.Drawing.Point(521, 261);
            this.comboBox3.Name = "comboBox3";
            this.comboBox3.Size = new System.Drawing.Size(131, 24);
            this.comboBox3.TabIndex = 16;
            this.comboBox3.Text = "  $";
            // 
            // labelSeparator
            // 
            this.labelSeparator.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.labelSeparator.Location = new System.Drawing.Point(372, 97);
            this.labelSeparator.Name = "labelSeparator";
            this.labelSeparator.Size = new System.Drawing.Size(1, 198);
            this.labelSeparator.TabIndex = 17;
            // 
            // menu
            // 
            this.menu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuFile,
            this.menuResult});
            this.menu.Location = new System.Drawing.Point(0, 0);
            this.menu.Name = "menu";
            this.menu.Size = new System.Drawing.Size(694, 24);
            this.menu.TabIndex = 3;
            this.menu.Text = "menuStrip1";
            // 
            // menuFile
            // 
            this.menuFile.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuFileImportTo,
            this.menuSeparator,
            this.menuExit});
            this.menuFile.Name = "menuFile";
            this.menuFile.Size = new System.Drawing.Size(48, 20);
            this.menuFile.Text = "Файл";
            // 
            // menuFileImportTo
            // 
            this.menuFileImportTo.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuFileImportToExcel,
            this.menuFileImportToTextDocument});
            this.menuFileImportTo.Name = "menuFileImportTo";
            this.menuFileImportTo.Size = new System.Drawing.Size(171, 22);
            this.menuFileImportTo.Text = "Импортировать в";
            // 
            // menuFileImportToTextDocument
            // 
            this.menuFileImportToTextDocument.Name = "menuFileImportToTextDocument";
            this.menuFileImportToTextDocument.Size = new System.Drawing.Size(186, 22);
            this.menuFileImportToTextDocument.Text = "текстовый документ";
            // 
            // menuFileImportToExcel
            // 
            this.menuFileImportToExcel.Name = "menuFileImportToExcel";
            this.menuFileImportToExcel.Size = new System.Drawing.Size(186, 22);
            this.menuFileImportToExcel.Text = "Excel";
            // 
            // menuSeparator
            // 
            this.menuSeparator.Name = "menuSeparator";
            this.menuSeparator.Size = new System.Drawing.Size(168, 6);
            // 
            // menuExit
            // 
            this.menuExit.Name = "menuExit";
            this.menuExit.Size = new System.Drawing.Size(171, 22);
            this.menuExit.Text = "Выход";
            // 
            // menuResult
            // 
            this.menuResult.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuResultOpenTable});
            this.menuResult.Name = "menuResult";
            this.menuResult.Size = new System.Drawing.Size(81, 20);
            this.menuResult.Text = "Результаты";
            // 
            // menuResultOpenTable
            // 
            this.menuResultOpenTable.Name = "menuResultOpenTable";
            this.menuResultOpenTable.Size = new System.Drawing.Size(238, 22);
            this.menuResultOpenTable.Text = "Открыть таблицу результатов";
            this.menuResultOpenTable.Click += new System.EventHandler(this.menuResultOpenTable_Click);
            // 
            // FormWebParserMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(694, 435);
            this.Controls.Add(this.panelSearch);
            this.Controls.Add(this.buttonParse);
            this.Controls.Add(this.menu);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menu;
            this.MaximizeBox = false;
            this.Name = "FormWebParserMain";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Web Parser";
            this.panelSearch.ResumeLayout(false);
            this.panelSearch.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericRoomsFrom)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericRoomsTo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericSquareTo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericSquareFrom)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericPriceTo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericPriceFrom)).EndInit();
            this.menu.ResumeLayout(false);
            this.menu.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button buttonParse;
        private System.Windows.Forms.Panel panelSearch;
        private System.Windows.Forms.ComboBox comboBoxSeach;
        private System.Windows.Forms.Label labelSearch;
        private System.Windows.Forms.ComboBox comboBoxTypes;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.Label lableOperation;
        private System.Windows.Forms.ComboBox comboBox2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label labelRooms;
        private System.Windows.Forms.Label labelTypePrice;
        private System.Windows.Forms.Label labelPrice;
        private System.Windows.Forms.Label labelSquare;
        private System.Windows.Forms.NumericUpDown numericSquareTo;
        private System.Windows.Forms.NumericUpDown numericSquareFrom;
        private System.Windows.Forms.NumericUpDown numericRoomsTo;
        private System.Windows.Forms.NumericUpDown numericRoomsFrom;
        private System.Windows.Forms.NumericUpDown numericPriceTo;
        private System.Windows.Forms.NumericUpDown numericPriceFrom;
        private System.Windows.Forms.ComboBox comboBox3;
        private System.Windows.Forms.Label labelSeparator;
        private System.Windows.Forms.MenuStrip menu;
        private System.Windows.Forms.ToolStripMenuItem menuFile;
        private System.Windows.Forms.ToolStripMenuItem menuFileImportTo;
        private System.Windows.Forms.ToolStripMenuItem menuFileImportToExcel;
        private System.Windows.Forms.ToolStripMenuItem menuFileImportToTextDocument;
        private System.Windows.Forms.ToolStripSeparator menuSeparator;
        private System.Windows.Forms.ToolStripMenuItem menuExit;
        private System.Windows.Forms.ToolStripMenuItem menuResult;
        private System.Windows.Forms.ToolStripMenuItem menuResultOpenTable;
    }
}

